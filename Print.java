public class Print {
    

    public void printMenu(){
        System.out.println("--------------------------------------------");
        System.out.println("=============SEJA BEM VINDO(A)==============");
        System.out.println("--------------------------------------------");
        System.out.println("1) Cadastrar nova disciplina");
        System.out.println("2) Cadastrar novo aluno");
        System.out.println("3) Matricular aluno ");
        System.out.println("4) Lançar notas de aluno");
        System.out.println("5) Mostrar Boletim de aluno");

    }
    public void printTipoDisciplina(){
        System.out.println("De que tipo será a disciplina?");
        System.out.println("1) DISCIPLINA PRÁTICA");
        System.out.println("2) DISCIPLINA TEÓRICA");
    }
    public void printSolicitarNomeAluno(){
        System.out.print("Digite o nome do aluno: ");
    }
    public void printSolicitarCodigoAluno(){
        System.out.print("Digite o codigo do aluno: ");
    }
    public void printSolicitarAnoLetivo(){
        System.out.print("Digite o ano Letivo do aluno: ");
    }
    public void printSolicitarSerie(){
        System.out.print("Digite a serie do aluno: ");
    }
    public void printAlunoNaoEncontrado(){
        System.out.println("Código de aluno não encontrado");
    }
    public void printDisciplinaNaoEncontrada(){

    }
    public void printDigiteNovamente(){
        System.out.println("Por Favor, digite novamente");
    }

    public void printSolicitaçãoDeCodigoDeDisciplina() {
        System.out.print("Digite o codigo da disciplina na qual o aluno será matriculado");
    }
    public void printImpossivelMatricular(){
        System.out.println("Impossível matricular aluno, é necessário que haja pelo menos um aluno e uma disciplina cadastrada");
    }
    public void printSolicitarBimestre(){
        System.out.print("digite o nome do semestre no qual a nota será adicionada");
    }
    public void printSolicitarCodigoDisciplina(){
        System.out.print("Digite o codigo da disciplina: ");
    }
    public void printAlunoOuDisciplinaNaoEcontrados(){
        System.out.println("Código de aluno ou código de disciplina não encontrado(s)");
    }
    public void printSolicitarNota(){
        System.out.print("Digite a nota a ser lançada: ");
    }
    public void printAtualizacaoDeNotas(){
        System.out.println("Nota atualizada com sucesso!");
    }
    public void printCodigoDeAlunoExistente(){
        System.out.println("O codigo de aluno digitado já existe!");
    }
    public void printCodigoDeDisciplinaExistente(){
        System.out.println("O codigo de Disciplina digitado já existe! ");
    }
    public void printSolicitarNomeDisciplina(){
        System.out.print("Digite o nome da disciplina: ");
    }
    public void printSolicitarBibliografia(){
        System.out.print("Digite o nome da bibliografia principal: ");
    }
    public void printSolicitarcargaHorariaTeorica(){
        System.out.print("Digite o número de horas de carga horaria teorica: ");
    }
    public void printSolicitarcargaHorariaPratica(){
        System.out.print("Digite o número de horas de carga horaria pratica: ");
    }
}
