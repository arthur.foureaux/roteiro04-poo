public class Aluno {
    private long codigo;
    private String nome;
    public Aluno(String nome, long codigo){
        this.nome = nome;
        this.codigo = codigo;
    }
    public void setCodigo(long codigo) {
        this.codigo = codigo;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public long getCodigo() {
        return codigo;
    }
    public String getNome() {
        return nome;
    }
}
