public class DisciplinaPrática extends Disciplina{
    private int cargaHorariaPratica;

    public DisciplinaPrática(long codigo, String nome, int cargaHoraria, int cargaHorariaPratica){
        super(codigo, nome, cargaHoraria);
        this.cargaHorariaPratica = cargaHorariaPratica;
    }
   
    public void setCargaHorariaPratica(int cargaHorariaPratica) {
        this.cargaHorariaPratica = cargaHorariaPratica;
    }
    public int getCargaHorariaPratica() {
        return cargaHorariaPratica;
    }
}
