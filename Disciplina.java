public class Disciplina {
    private long codigo;
    private String nome;
    private int cargaHoraria;
    public Disciplina(long codigo, String nome, int cargaHoraria){
        this.codigo = codigo;
        this.nome = nome;
        this.cargaHoraria = cargaHoraria;
    }
    public void setCargaHoraria(int cargaHoraria) {
        this.cargaHoraria = cargaHoraria;
    }
    public void setCodigo(long codigo) {
        this.codigo = codigo;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public int getCargaHoraria() {
        return cargaHoraria;
    }
    public long getCodigo() {
        return codigo;
    }
    public String getNome() {
        return nome;
    }
}
