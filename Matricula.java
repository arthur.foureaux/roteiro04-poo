public class Matricula {
    private Aluno aluno;
    private int anoLetivo;
    private Disciplina disciplina;
    private int serie;
    private double[] notas;

    public Matricula(Aluno aluno, int anoLetivo, Disciplina disciplina, int serie){
        this.aluno = aluno;
        this.anoLetivo = anoLetivo;
        this.disciplina = disciplina;
        this.serie = serie;
        notas = new double[4];
    }

    public Aluno getAluno() {
        return aluno;
    }
    public Disciplina getDisciplina() {
        return disciplina;
    }
    public void setNotas(double notas, int bimestre) {
        this.notas[bimestre-1] += notas;
    }
    
}

