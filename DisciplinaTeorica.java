public class DisciplinaTeorica extends Disciplina {
    String bibliografiaPrincipal;
    public DisciplinaTeorica(long codigo, String nome, int cargaHoraria, String bibliografiaPrincipal){
        super(codigo, nome, cargaHoraria);
        this.bibliografiaPrincipal = bibliografiaPrincipal;
    }
    
}
