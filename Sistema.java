import java.util.Scanner;
import java.util.ArrayList;
public class Sistema {

    private Print face;
    private Scanner in;
    private ArrayList<Disciplina> disciplinas;
    private ArrayList<Matricula> matriculas;
    private ArrayList<Aluno> alunos;

    public Sistema(){
        face = new Print();
        in = new Scanner(System.in);
        disciplinas = new ArrayList<Disciplina>();
    }

    public static void main(String[] args) {
        Sistema sistema = new Sistema();
        sistema.start();
    }

    

    public void start(){
        int option;
        boolean continuar = true;
        while (continuar) {
            face.printMenu();
        option = in.nextInt();
        switch (option) {
            case 1:
                disciplinas.add(cadastroDeDisciplina()) ;
                break;
            case 2:
                alunos.add(cadastrarNovoAluno());
                break;
            case 3:
                matriculas.add(matricularAlunoEmDisciplina());
                break;
            case 4:
                modificarNota();
                break;
            case 5:
                
                break;
            case 6:
                continuar = false;
                break;
        
            default:
                System.out.print("Opção inválida!");
                break;
            }
        }
        
        
    }
    public Disciplina cadastroDeDisciplina(){
        int aux;
        Disciplina novaDisciplina;
        face.printTipoDisciplina();
        aux = in.nextInt();
        String nomeDisciplina;
        int codigoDisciplina, cargaHoraria;
        boolean jaExiste = false;
        switch (aux) {
            case 1:
                int cargaHorariaPratica;
                face.printSolicitarNomeDisciplina();
                nomeDisciplina = in.nextLine();
                face.printSolicitarCodigoDisciplina();
                codigoDisciplina = in.nextInt();
                face.printSolicitarcargaHorariaTeorica();
                cargaHoraria = in.nextInt();
                face.printSolicitarcargaHorariaPratica();
                cargaHorariaPratica = in.nextInt();
                for (Disciplina disciplina : disciplinas) {
                    if (disciplina.getCodigo() == codigoDisciplina) {
                        face.printCodigoDeDisciplinaExistente();
                        jaExiste = true;
                    }
                }
                if (!jaExiste) {
                    novaDisciplina = new DisciplinaPrática(codigoDisciplina, nomeDisciplina, cargaHoraria, cargaHorariaPratica);    
                } else{
                    face.printCodigoDeDisciplinaExistente();
                    return null;
                }
                
                return novaDisciplina;
            case 2:
                String bibliografiaPrincipal;
                face.printSolicitarBibliografia();
                bibliografiaPrincipal = in.nextLine();
                face.printSolicitarNomeDisciplina();
                nomeDisciplina = in.nextLine();
                face.printSolicitarCodigoDisciplina();
                codigoDisciplina = in.nextInt();
                face.printSolicitarcargaHorariaTeorica();
                cargaHoraria = in.nextInt();
                for (Disciplina disciplina : disciplinas) {
                    if (disciplina.getCodigo() == codigoDisciplina) {
                        face.printCodigoDeDisciplinaExistente();
                        jaExiste = true;
                    }
                }
                if (!jaExiste) {
                    novaDisciplina = new DisciplinaTeorica(codigoDisciplina, nomeDisciplina, cargaHoraria, bibliografiaPrincipal);
                } else{
                    face.printCodigoDeDisciplinaExistente();
                    return null;
                }
                return novaDisciplina;
            default:
                System.out.print("Opção inválida!");
                return null;
        }       
    }
    public Aluno cadastrarNovoAluno(){
        face.printSolicitarNomeAluno();
        String nome = in.nextLine();
        face.printSolicitarCodigoAluno();
        long codigo = in.nextLong();
        boolean jaExiste = false;
        for (Aluno aluno : alunos) {
            if (codigo == aluno.getCodigo()) {
                jaExiste = true;
            }
        }
        if (!jaExiste) {
            Aluno novoAluno = new Aluno(nome, codigo);
            in.nextLine();
            return novoAluno;
        } else{
            face.printCodigoDeAlunoExistente();
            return null;
        }
        

    }
    public Matricula matricularAlunoEmDisciplina(){
        Matricula novaMatricula;
        boolean aux = true;
        int indexAluno=0;
        int indexDisciplina=0;
        if (alunos.size() == 0 || disciplinas.size() == 0) {
            face.printImpossivelMatricular();
            return null;
        } else{
            while(aux){
                face.printSolicitarCodigoAluno();
                int supostoCodigoDeAluno = in.nextInt();
                for (Aluno aluno : alunos) {
                    if (aluno.getCodigo()==supostoCodigoDeAluno) {
                        indexAluno = alunos.indexOf(aluno);
                        aux = false;
                    }
                }
                if (!aux) {
                    face.printAlunoNaoEncontrado();
                    face.printDigiteNovamente();
                }  
                in.nextLine(); //limpa o buffer
            }
            aux = true;
            while (aux) {
                face.printSolicitaçãoDeCodigoDeDisciplina();
                int supostoCodigoDeDisciplina = in.nextInt();
                for (Disciplina disciplina : disciplinas) {
                    if (supostoCodigoDeDisciplina==disciplina.getCodigo()) {
                        indexDisciplina = disciplinas.indexOf(disciplina);
                        aux = false;
                    }
                }
                if (!aux) {
                    face.printDisciplinaNaoEncontrada();
                    face.printDigiteNovamente();
                } 
                in.nextLine();
            }
            face.printSolicitarAnoLetivo();
            int anoLetivo = in.nextInt();
            face.printSolicitarNomeAluno();
            int serie = in.nextInt();
            in.nextLine(); // limpeza do buffer do Scanner
            novaMatricula = new Matricula(alunos.get(indexAluno), anoLetivo, disciplinas.get(indexDisciplina),  serie);
            return novaMatricula;
        }
        
    }
    public void modificarNota(){
        int indexMatricula = 0, codigoAluno, bimestre, codigoDisciplina, ano; 
        double nota;
        boolean valid = false;
        
        face.printSolicitarCodigoAluno();
        codigoAluno = in.nextInt();
        face.printSolicitarCodigoDisciplina();
        codigoDisciplina = in.nextInt();
        face.printSolicitarAnoLetivo();
        ano = in.nextInt();
        face.printSolicitarBimestre();
        bimestre = in.nextInt();
        for (Aluno aluno : alunos) {
            if (codigoAluno == aluno.getCodigo()) {
               for (Disciplina disciplina : disciplinas) {
                   if (codigoDisciplina == disciplina.getCodigo()) {
                       valid = true;
                   }
               }
            } 
        }
        if (!valid) {
            face.printAlunoOuDisciplinaNaoEcontrados();
        } else{
            Matricula matriculaModificavel;
            for (Matricula matricula : matriculas) {
                if (codigoAluno == matricula.getAluno().getCodigo() && codigoDisciplina == matricula.getDisciplina().getCodigo() ) {
                    indexMatricula = matriculas.indexOf(matricula);
                }
            }
            face.printSolicitarNota();
            nota = in.nextDouble();
            matriculas.get(indexMatricula).setNotas(nota, bimestre);
            face.printAtualizacaoDeNotas();
        }

    }
}
